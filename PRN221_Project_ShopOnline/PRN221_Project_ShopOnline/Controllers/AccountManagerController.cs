﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using PRN221_Project_ShopOnline.DAO;
using PRN221_Project_ShopOnline.Models;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Text.Json;
using System.Net.Http.Json;

namespace PRN221_Project_ShopOnline.Controllers
{
    public class AccountManagerController : Controller
    {
        private readonly HttpClient client = null;
        private string url = "";
        private string urlc = "";
        public AccountManagerController()
        {
            client = new HttpClient();
            var contentType = new MediaTypeWithQualityHeaderValue("application/json");
            client.DefaultRequestHeaders.Accept.Add(contentType);
            url = "https://localhost:7007/AccountManager";
        }
        public async Task<IActionResult> Index()
        {
            await SetDataToView();

            return View("Views/AccountManager.cshtml");
        }

        public async Task SetDataToView()
        {
            //Get data from DB
            var response = await client.GetAsync(url);
            var json = await response.Content.ReadAsStringAsync();
            var options = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };
            if (!response.IsSuccessStatusCode)
            {

                ViewBag.Message = "Something went wrong! Please try again";
            }
            var users = JsonSerializer.Deserialize<List<User>>(json, options);
            //Set data to View
            ViewBag.Users = users;
        }

        [HttpPost]
        public async Task<IActionResult> DeleteAccount(int UserId)
        {
            HttpResponseMessage response = await client.DeleteAsync($"{url}/Delete/{UserId}");
            //if Delete fail -> notify
            if (!response.IsSuccessStatusCode)
            {
                ViewBag.Message = "Something went wrong! Please try again";
            }

            //Back to Manage Product
            await SetDataToView();
            return View("Views/AccountManager.cshtml");
        }
        //Go to Edit Form
        public async Task<IActionResult> EditAccount(int UserID)
        {
            //Get account to edit
            try
            {
                var response = await client.GetAsync(url + "/EditAccount/" + UserID);
                var json = await response.Content.ReadAsStringAsync();
                var options = new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                };
                if (!response.IsSuccessStatusCode)
                {

                    ViewBag.Message = "Something went wrong! Please try again";
                }
                var users = JsonSerializer.Deserialize<User>(json, options);
                //Set data to View
                ViewBag.Users = users;

                return View("Views/EditAccount.cshtml");
            }
            catch (Exception ex)
            {
                throw new Exception();
            }

        }

        //Update Account to DB
 
        [HttpPost]
        public async Task<IActionResult> UpdateAccount(int id, string username, string email, string password, int IsSeller, int IsAdmin)
        {
            try
            {
                //Set data
                User user = new User();
                user.UserId = id;
                user.Username = username;
                user.Email = email;
                user.Password = password;
                user.IsSeller = IsSeller;
                user.IsAdmin = IsAdmin;

                //Update to DB
                JsonContent content = JsonContent.Create(user);
                var response = await client.PostAsync(url+"/UpdateAccount",content);
                var json = await response.Content.ReadAsStringAsync();
                var options = new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                };
                if (!response.IsSuccessStatusCode)
                {

                    ViewBag.Message = "Something went wrong! Please try again";
                }
            }
            catch (Exception)
            {
                //Wrong input -> notify
                ViewBag.Message = "Something went wrong! Please try again";
            }

            //Back to ManageAccount
            await SetDataToView();
            return View("Views/AccountManager.cshtml");
        }
    }
}