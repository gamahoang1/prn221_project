﻿using Microsoft.AspNetCore.Mvc;

using PRN221_Project_ShopOnline.DAO;
using PRN221_Project_ShopOnline.Models;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Threading.Tasks;

namespace PRN221_Project_ShopOnline.Controllers
{
    public class DashboardController : Controller
    {
        private readonly HttpClient client = null;
        private string url = "";

        public DashboardController()
        {
            client = new HttpClient();
            var contentType = new MediaTypeWithQualityHeaderValue("application/json");
            client.DefaultRequestHeaders.Accept.Add(contentType);
            url = "https://localhost:7007/Sale";

        }
        public async Task<IActionResult> Index()
        {
             await SetDataToView();

            var view = View("Views/SaleDashboard.cshtml");
            return view;
        }

        private async Task SetDataToView()
        {
            //num account, num product, num order, top 3 most sell, top 3 least sell, recent order, count prod by cat
            //num account
            var response = await client.GetAsync(url+ "/numAcc");
            var json = await response.Content.ReadAsStringAsync();
            var options = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };
            if (!response.IsSuccessStatusCode)
            {
                 NotFound();
            }
            double numAcc = JsonSerializer.Deserialize<double>(json, options);
            ViewBag.NumAcc = numAcc;

            //num product
            var response1 = await client.GetAsync(url+ "/numProd/");
            var json1 = await response1.Content.ReadAsStringAsync();
            var options1 = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };
            if (!response1.IsSuccessStatusCode)
            {
                NotFound();
            }
            double numProd = JsonSerializer.Deserialize<double>(json1, options1);           
            ViewBag.NumProd = numProd;

            //num order
            var response2 = await client.GetAsync(url + "/numOrd");
            var json2 = await response2.Content.ReadAsStringAsync();
            var options2 = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };
            if (!response2.IsSuccessStatusCode)
            {
                NotFound();
            }
            double numOrd = JsonSerializer.Deserialize<double>(json2, options2);

            ViewBag.NumOrd = numOrd;
            ElectronicShopPRN221Context context = new ElectronicShopPRN221Context();
            var q = (from h in context.OrderDetails
                     group h by new { h.ProductId } into hh
                     select new OrderDetail
                     {
                         ProductId = hh.Key.ProductId,
                         Quantity = hh.Sum(s => s.Quantity)
                     }).OrderByDescending(i => i.Quantity).ToList<OrderDetail>();
            List<int> a = new List<int>();
            for (int i = 0; i < 3; i++)
            {
                a.Add(q[i].ProductId);
            }
            List<Product> mostSellProducts = (List<Product>)context.Products.Where(p => a.Contains(p.ProductId)).ToList();
            ViewBag.MostSellProd = mostSellProducts;

            // count prod by cate
            var prodCat = (from h in context.Products
                           group h by new { h.CategoryId } into hh
                           select new Product
                           {
                               CategoryId = hh.Key.CategoryId,
                               Amount = hh.Count()
                           }).ToList<Product>();
            
            foreach (Product prod in prodCat)
            {
                prod.ProductName = context.Categories.FirstOrDefault(c => c.CategoryId == prod.CategoryId).CategoryName;
                
            }
           
            ViewBag.ProdPerCate = prodCat;

            //total revenue
            int totalRevenue = 0;
            var response3 = await client.GetAsync(url + "/totalRevenue");
            var json3 = await response3.Content.ReadAsStringAsync();
            var options3 = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };
            if (!response3.IsSuccessStatusCode)
            {
                NotFound();
            }
            totalRevenue  = JsonSerializer.Deserialize<int>(json3, options3);
            ViewBag.TotalRevenue = totalRevenue;

        }
    }
}
