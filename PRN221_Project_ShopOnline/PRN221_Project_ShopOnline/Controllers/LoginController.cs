﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using PRN221_Project_ShopOnline.DAO;
using PRN221_Project_ShopOnline.Models;

using Microsoft.AspNetCore.Http;
using System.Net.Http.Json;
using System.Security.Policy;
using System.Text.Json;
using System.Net.Http.Headers;
using System.Net.Http;

namespace PRN221_Project_ShopOnline.Controllers
{
    public class LoginController : Controller
    {
        private readonly HttpClient client = null;
        private string url = "";
        public LoginController()
        {
            client = new HttpClient();
            var contentType = new MediaTypeWithQualityHeaderValue("application/json");
            client.DefaultRequestHeaders.Accept.Add(contentType);
            url = "https://localhost:7007/user";
        }
        public IActionResult Index()
        {
            //if has Cookies -> set username & password for auto login
            ViewBag.CookieUsername = "";
            ViewBag.CookiePassword = "";
            if (HttpContext.Request.Cookies.ContainsKey("username"))
            {
                ViewBag.CookieUsername = HttpContext.Request.Cookies["username"];
                ViewBag.CookiePassword = HttpContext.Request.Cookies["password"];
            }

            return View("Views/Login.cshtml");
        }

        [HttpPost]
        public async Task<IActionResult> Login(User u, string remember)
        {

            //Check login account with Database

            JsonContent content = JsonContent.Create(u);
            var response = await client.PostAsync(url + "/login/", content);
            if (!response.IsSuccessStatusCode)

            {
                //user not exists -> back to Login
                ViewBag.Message = "Wrong username or password";
                return View("Views/Login.cshtml");
            }
            else
            {
                var response1 = await client.GetAsync(url + "/username/"+u.Username);
                var json1 = await response1.Content.ReadAsStringAsync();
                var options1 = new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                };
                var user = JsonSerializer.Deserialize<User>(json1, options1);
                //login success: 

                //set User to Session (for other pages to use)
                HttpContext.Session.SetString("username", user.Username);
                HttpContext.Session.SetString("password", user.Password);
                HttpContext.Session.SetInt32("userId", user.UserId);
                HttpContext.Session.SetInt32("isSeller", (int)user.IsSeller);
                HttpContext.Session.SetInt32("isAdmin", (int)user.IsAdmin);

                //set User to Cookies (for auto Login)
                if (!HttpContext.Request.Cookies.ContainsKey("userId"))
                {
                    CookieOptions userOptions = new CookieOptions();
                    userOptions.Expires = new DateTimeOffset(DateTime.Now.AddHours(1)); //cookie last for 1 hour
                    HttpContext.Response.Cookies.Append("username", u.Username, userOptions);

                    CookieOptions passwordOptions = new CookieOptions();
                    //if user choose [Remember Me] -> save password
                    if (remember != null)
                    {
                        passwordOptions.Expires = new DateTimeOffset(DateTime.Now.AddHours(1));
                    }
                    else
                    {
                        passwordOptions.Expires = new DateTimeOffset(DateTime.Now);
                    }
                    HttpContext.Response.Cookies.Append("password", user.Password);
                }

                //to Home Page
                return Redirect("/");
            }
        }

        public IActionResult Logout()
        {
            //Remove Session
            HttpContext.Session.Remove("userId");
            HttpContext.Session.Remove("username");
            HttpContext.Session.Remove("password");

            //back to home
            return Redirect("/");
        }

        public async Task<IActionResult> Signup(string username, string email, string password, string repassword)
        {
            try
            {
                if (password.Equals(repassword)){
                    User u = new User();
                    u.Email = email;
                    u.Password = password;
                    u.Username = username;
                    JsonContent content = JsonContent.Create(u);
                    var response = await client.PostAsync(url + "/Signup/", content);
                    //if Add fail -> throw exception to notify
                    if (!response.IsSuccessStatusCode)
                    {
                        ViewBag.Message = "User or Email exsit";
                    }
                }
                else
                {
                    ViewBag.Message = "Password Incorrect! ";
                }
                
            }catch (Exception ex) 
            {
                //Wrong input -> notify
                ViewBag.Message = ex;
            }
            return View("Views/Login.cshtml");
        }
    }
}
