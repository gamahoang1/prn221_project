﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using PRN221_Project_ShopOnline.Models;
using PRN221_Project_ShopOnline.DAO;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.Extensions.Configuration;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Text.Json;
using System.Net.Http.Json;

namespace PRN221_Project_ShopOnline.Controllers
{
    public class ProductManagerController : Controller
    {
        private readonly HttpClient client = null;
        private string url = "";
        private string urlc = "";
        public ProductManagerController()
        {
            client = new HttpClient();
            var contentType = new MediaTypeWithQualityHeaderValue("application/json");
            client.DefaultRequestHeaders.Accept.Add(contentType);
            url = "https://localhost:7007/product";
            urlc = "https://localhost:7007/category";
        }
        public async Task<IActionResult> Index()
        {
            await SetDataToViewAsync();

            return View("Views/ProductManager.cshtml");
        }

        //Make this a different method because there is reuse
        private async Task SetDataToViewAsync()
        {
            //Get info of this Seller from Session
            int sellerId = (int)HttpContext.Session.GetInt32("userId");
            string sellerName = HttpContext.Session.GetString("username");

            var response = await client.GetAsync(url + "/sellerId/" + sellerId);
            var json = await response.Content.ReadAsStringAsync();
            var options = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };
            var products = JsonSerializer.Deserialize<List<Product>>(json, options);
            var responsec = await client.GetAsync(urlc);
            var jsonc = await responsec.Content.ReadAsStringAsync();
            var optionsc = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };
            var categories = JsonSerializer.Deserialize<List<Category>>(jsonc, optionsc);

            //Set data to View
            ViewBag.Products = products;
            ViewBag.Categories = categories;
            //User info for Seller in [Create Product]
            ViewBag.SellerId = sellerId;
            ViewBag.SellerName = sellerName;
        }

        [HttpPost]
        public async Task<IActionResult> AddProduct(string name, string description, int price, 
            IFormFile image, int CategoryID, int SellerID, int amount)
        {
            try
            {
                //Get Image Path from JSON
                var config = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
                string rootdir = config.GetSection("ImageRootPath").Value.ToString();

                //Copy Image file to Project Folder
                string filename = Path.Combine(rootdir, image.FileName);

                try
                {
                    using (var fs = new FileStream(filename, FileMode.Create))
                    {
                        image.CopyTo(fs);
                    }
                    Console.WriteLine("File saved successfully.");
                }
                catch (IOException ex)
                {
                    Console.WriteLine($"Error saving file: {ex.Message}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Unexpected error: {ex.Message}");
                }

                //Set data
                Product product = new Product();
                product.ProductName = name;
                product.Description = description;
                product.SellPrice = price;
                product.ImageLink = image.FileName;
                product.CategoryId = CategoryID;
                product.SellerId = SellerID;
                product.Amount = amount;

                //Add to DB
                JsonContent content = JsonContent.Create(product);
                var response = await client.PostAsync($"{url}/ProductManager", content);
                //if Add fail -> throw exception to notify
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception();
                }
            } catch (Exception)
            {
                //Wrong input -> notify
                ViewBag.Message = "Wrong input! Please try again";
            }

            //Back to Manage Product
            await SetDataToViewAsync();

            return View("Views/ProductManager.cshtml");
        }

        //Go to Edit Form
        public async Task<IActionResult> EditProduct(int productId)
        {
            //Get info of this Seller from Session
            int sellerId = (int)HttpContext.Session.GetInt32("userId");
            string sellerName = HttpContext.Session.GetString("username");

            //Get product to edit
            var response = await client.GetAsync(url + "/id/" + productId);
            var json = await response.Content.ReadAsStringAsync();
            var options = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };
            var products = JsonSerializer.Deserialize<Product>(json, options);

            //Get list category for [Edit Product]
            var responsec = await client.GetAsync(urlc);
            var jsonc = await responsec.Content.ReadAsStringAsync();
            var optionsc = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };
            var categories = JsonSerializer.Deserialize<List<Category>>(jsonc, optionsc);

            //Set data to View
            ViewBag.Product = products;
            ViewBag.Categories = categories;
            //User info for Seller in [Edit Product]
            ViewBag.SellerId = sellerId;
            ViewBag.SellerName = sellerName;

            return View("Views/EditProduct.cshtml");
        }

        //Update Product in DB
        [HttpPost]
        public async Task<IActionResult> UpdateProduct(int id, string name, string description, int price,
            IFormFile image, int CategoryID, int SellerID, int amount)
        {
            try
            {
                //if user does not change image -> Keep old image
                if (image == null)
                {
                    //Get old product
                    ProductDAO productDAO = new ProductDAO();
                    Product oldProduct = productDAO.GetProductByID(id);

                    //Set data
                    Product product = new Product();
                    product.ProductId = id;
                    product.ProductName = name;
                    product.Description = description;
                    product.SellPrice = price;
                    product.ImageLink = oldProduct.ImageLink;   //not change image
                    product.CategoryId = CategoryID;
                    product.SellerId = SellerID;
                    product.Amount = amount;

                    //Update to DB
                    ProductDAO dao = new ProductDAO();
                    dao.EditProduct(product);
                }
                else
                {
                    //Get Image Path from JSON
                    var config = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
                    string rootdir = config.GetSection("ImageRootPath").Value.ToString();

                    //Copy Image file to Project Folder
                    string filename = Path.Combine(rootdir, image.FileName);

                    try
                    {
                        using (var fs = new FileStream(filename, FileMode.Create))
                        {
                            image.CopyTo(fs);
                        }
                        Console.WriteLine("File saved successfully.");
                    }
                    catch (IOException ex)
                    {
                        Console.WriteLine($"Error saving file: {ex.Message}");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"Unexpected error: {ex.Message}");
                    }

                    //Set data
                    Product product = new Product();
                    product.ProductId = id;
                    product.ProductName = name;
                    product.Description = description;
                    product.SellPrice = price;
                    product.ImageLink = image.FileName;
                    product.CategoryId = CategoryID;
                    product.SellerId = SellerID;
                    product.Amount = amount;

                    //Edit PRoduct
                    JsonContent content = JsonContent.Create(product);
                    var response = await client.PutAsync($"{url}/edit/ProductManager/{product.ProductId}", content);
                    if (!response.IsSuccessStatusCode)
                    {
                        throw new Exception();
                    }
                }
            }
            catch (Exception)
            {
                //Wrong input -> notify
                ViewBag.Message = "Wrong input! Please try again";
            }

            //Back to Manage Product
            await SetDataToViewAsync();

            return View("Views/ProductManager.cshtml");
        }

        //Delete product in DB
        [HttpPost]
        public async Task<IActionResult> DeleteProduct(int ProductID)
        {
            //Delete in DB
            HttpResponseMessage response = await client.DeleteAsync($"{url}/delete/ProductManager/{ProductID}");
            //if Delete fail -> notify
            if (!response.IsSuccessStatusCode)
            {
                ViewBag.Message = "Something went wrong! Please try again";
            }

            //Back to Manage Product
            await SetDataToViewAsync();

            return View("Views/ProductManager.cshtml");
        }
    }
}
