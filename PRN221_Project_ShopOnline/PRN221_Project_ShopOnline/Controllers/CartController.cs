﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using PRN221_Project_ShopOnline.DAO;
using PRN221_Project_ShopOnline.Models;

using Microsoft.AspNetCore.Http;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;

namespace PRN221_Project_ShopOnline.Controllers
{
    public class CartController : Controller
    {
        private readonly HttpClient client = null;
        private string url = "";
        private string urlc = "";
        private string urlcc = "";
        private string urlccc = "";
        public CartController()
        {
            client = new HttpClient();
            var contentType = new MediaTypeWithQualityHeaderValue("application/json");
            client.DefaultRequestHeaders.Accept.Add(contentType);
            url = "https://localhost:7007/cart";
            urlc = "https://localhost:7007/category";
            urlcc = "https://localhost:7007/product";
            urlccc = "https://localhost:7007/AccountManager";
        }
        //Show List Cart
        public async Task<IActionResult> Index()
        {
            //Get user Id fron Session
            int UserId = (int)HttpContext.Session.GetInt32("userId");

            //Get list Cart from DB
            var response = await client.GetAsync(url + "/GetCartsOfUser/"+ UserId);
            var json = await response.Content.ReadAsStringAsync();
            var options = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };
            if (!response.IsSuccessStatusCode)
            {
                return NotFound("No Cart");
            }
            var carts = JsonSerializer.Deserialize<List<Cart>>(json, options);
            
            //count total Price
            int totalPrice = 0;
            foreach (Cart cart in carts)
            {
                //find the Product of each Cart for display
                /*                var response1 = await client.GetAsync(url + "/GetProductsOfCartOfUser/"+ cart.ProductId);
                                var json1 = await response1.Content.ReadAsStringAsync();
                                var options1 = new JsonSerializerOptions
                                {
                                    PropertyNameCaseInsensitive = true
                                };
                                var productofcart = JsonSerializer.Deserialize<Product>(json1, options1);         
                                totalPrice += (int)productofcart.SellPrice * (int)cart.Amount;*/
                ProductDAO productDAO = new ProductDAO();
                cart.Product = productDAO.GetProductByID(cart.ProductId);
                totalPrice += (int)cart.Product.SellPrice * (int)cart.Amount;
            }

            //set data to View
            ViewBag.Carts = carts;
            ViewBag.TotalPrice = GetPriceWithDot(totalPrice);

            return View("Views/ShowCart.cshtml");
        }

        //For display in Index()
        public String GetPriceWithDot(int price)
        {
            return price.ToString("N0");
        }

        //Add 1 Product to Cart (Amount=1) {From: Home}
        
        public async Task<IActionResult> AddToCart(int productId)
        {
            /*----------Add To Cart----------*/
            //Use Session to get user id
            int UserId = 0;
            try
            {
                UserId = (int)HttpContext.Session.GetInt32("userId");
            } catch (Exception)
            {
                //If Exception -> User is not Login (can't add to cart) -> go to Login Page
                return Redirect("/Login/Index");
            }


            //Add to DB (amount = 1)
            var response = await client.GetAsync(url + "/addtocart/"+ productId+"/"+ UserId);
            var json = await response.Content.ReadAsStringAsync();
            var options = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };
            if (!response.IsSuccessStatusCode)
            {
                return NotFound();
            }
            bool notOutOfStock = JsonSerializer.Deserialize<bool>(json, options);
          
            //Notify Result
            if (notOutOfStock)
            {
                ViewBag.AddToCartMessage = "Product is added to cart";
            } else
            {
                ViewBag.AddToCartMessage = "Sorry, there is not enough Amount in Stock";
            }

            /*----------Back to Home Page----------*/
            var view = View("Views/Index.cshtml");

            var response1 = await client.GetAsync(urlcc);
            var json1 = await response1.Content.ReadAsStringAsync();
            var options1 = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };
            var products = JsonSerializer.Deserialize<List<Product>>(json1, options1);
            var responsec = await client.GetAsync(urlc);
            var jsonc = await responsec.Content.ReadAsStringAsync();
            var optionsc = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };
            var categories = JsonSerializer.Deserialize<List<Category>>(jsonc, optionsc);

            //set to ViewBag
            ViewBag.Categories = categories;
            ViewBag.Products = products;
            //no selected category
            ViewBag.SelectedCategory = 0;

            return view;
        }

        //Add an Amount of Product to Cart {From: ProductDetail}
        [HttpPost]
        public async Task<IActionResult> AddManyToCart(int ProductId, int Amount)
        {
            /*---Add to Cart---*/
            //Use Session to get user id
            int UserId = 0;
            try
            {
                UserId = (int)HttpContext.Session.GetInt32("userId");
            }
            catch (Exception)
            {
                //If Exception -> User is not Login (can't add to cart) -> go to Login Page
                return Redirect("/Login/Index");
            }

            //Add to DB
            var response = await client.GetAsync(url + "/addtocart/" + ProductId + "/" + UserId+"/"+ Amount);
            var json = await response.Content.ReadAsStringAsync();
            var options = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };
            if (!response.IsSuccessStatusCode)
            {
                return NotFound();
            }
            bool notOutOfStock = JsonSerializer.Deserialize<bool>(json, options);

            //Notify Result
            if (notOutOfStock)
            {
                ViewBag.AddToCartMessage = "Product is added to cart";
            }
            else
            {
                ViewBag.AddToCartMessage = "Sorry, there is not enough Amount in Stock";
            }

            /*----------Back to Home Page----------*/
            var view = View("Views/Index.cshtml");

            var response1 = await client.GetAsync(urlcc);
            var json1 = await response1.Content.ReadAsStringAsync();
            var options1 = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };
            var products = JsonSerializer.Deserialize<List<Product>>(json1, options1);
            var responsec = await client.GetAsync(urlc);
            var jsonc = await responsec.Content.ReadAsStringAsync();
            var optionsc = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };
            var categories = JsonSerializer.Deserialize<List<Category>>(jsonc, optionsc);

            //set to ViewBag
            ViewBag.Categories = categories;
            ViewBag.Products = products;
            //no selected category
            ViewBag.SelectedCategory = 0;

            return view;
        }

        //Delete all Products in Cart of 1 User
        public async Task<IActionResult> DeleteCart()
        {
            //id of user from session
            int UserId = (int)HttpContext.Session.GetInt32("userId");
            HttpResponseMessage response = await client.DeleteAsync($"{url}/DeleteProductInCart/{UserId}");
            //if Delete fail -> notify
            if (!response.IsSuccessStatusCode)
            {
                ViewBag.Message = "Something went wrong! Please try again";
            }

            //back to show cart
            return Redirect("/Cart/Index");
        }

        //Delete 1 Item in Cart of 1 User
        
        public async Task<IActionResult> DeleteProductInCart(int ProductID)
        {
            //id of user from session
            int UserId = (int)HttpContext.Session.GetInt32("userId");

            //Delete cart item
            HttpResponseMessage response = await client.DeleteAsync($"{url}/DeleteProductInCart/{ProductID}/{UserId}");
            //if Delete fail -> notify
            if (!response.IsSuccessStatusCode)
            {
                ViewBag.Message = "Something went wrong! Please try again";
            }

            //back to show cart
            return Redirect("/Cart/Index");
        }

        //---------------Buy---------------
        public async Task<IActionResult> Buy()
        {
            //Get user Id fron Session
            int UserId = (int)HttpContext.Session.GetInt32("userId");

            //Get list Cart from DB
            var response = await client.GetAsync(url + "/GetCartsOfUser/" + UserId);
            var json = await response.Content.ReadAsStringAsync();
            var options = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };
            if (!response.IsSuccessStatusCode)
            {
                return NotFound("No Cart");
            }
            var carts = JsonSerializer.Deserialize<List<Cart>>(json, options);

            //if doesn't have any item in cart => back to Show Cart
            if (carts.Count == 0)
            {
                return Redirect("/Cart/Index");
            }

            //ship info
            var response1 = await client.GetAsync(url + "/Ship/" + UserId);
            var json1 = await response1.Content.ReadAsStringAsync();
            var options1 = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };
            if (!response.IsSuccessStatusCode)
            {
                return NotFound("No Cart");
            }
            var ships = JsonSerializer.Deserialize<List<Ship>>(json1, options1);
            //Count total price
            int totalPrice = 0;
            foreach (Cart cart in carts)
            {
                //find the Product of each Cart for display
                ProductDAO productDAO = new ProductDAO();
                cart.Product = productDAO.GetProductByID(cart.ProductId);

                totalPrice += (int)cart.Product.SellPrice * (int)cart.Amount;
            }

            //Get User Email
            var response2 = await client.GetAsync(urlccc + "/EditAccount/" + UserId);
            var json2 = await response2.Content.ReadAsStringAsync();
            var options2 = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };
            if (!response.IsSuccessStatusCode)
            {
                return NotFound("No Cart");
            }
            var user = JsonSerializer.Deserialize<User>(json2, options2);
            string email = user.Email;

            //set to view
            ViewBag.Carts = carts;
            ViewBag.Ships = ships;
            ViewBag.TotalPrice = GetPriceWithDot(totalPrice);
            ViewBag.CountNumCart = carts.Count;
            ViewBag.UserEmail = email;

            return View("Views/Buy.cshtml");
        }

        //---------------Finish---------------
        [HttpPost]
        public async Task<IActionResult> Finish()
        {
            /*todo: add Cart to Order*/

            /*Delete Cart*/
            //Get user from session
            int UserId = (int)HttpContext.Session.GetInt32("userId");
            //delete
            HttpResponseMessage response = await client.DeleteAsync($"{url}/Finish/{UserId}");
            //if Delete fail -> notify
            if (!response.IsSuccessStatusCode)
            {
                ViewBag.Message = "Something went wrong! Please try again";
            }

            return View("Views/Finish.cshtml");
        }
    }
}
