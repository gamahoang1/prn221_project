﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PRN221_Project_ShopOnline.DAO;
using PRN221_Project_ShopOnline.Models;
using Microsoft.AspNetCore.Http;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Text.Json;

namespace PRN221_Project_ShopOnline.Controllers
{
    public class ProductDetailController : Controller
    {
        private readonly HttpClient client = null;
        private string url = "";

        public ProductDetailController()
        {
            client = new HttpClient();
            var contentType = new MediaTypeWithQualityHeaderValue("application/json");
            client.DefaultRequestHeaders.Accept.Add(contentType);
            url = "https://localhost:7007/product";
        }
        public async Task<ActionResult> Index(int ProductID)
        {
            var view = View("Views/ProductDetail.cshtml");

            //Get product Detail
            var response = await client.GetAsync(url+"/id/"+ProductID);
            var json = await response.Content.ReadAsStringAsync();
            var options = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };
            var products = JsonSerializer.Deserialize<Product>(json, options);

            //set to ViewBag
            ViewBag.Product = products;

            return view;
        }
    }
}
