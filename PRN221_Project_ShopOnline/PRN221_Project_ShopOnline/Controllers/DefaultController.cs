﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using PRN221_Project_ShopOnline.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using PRN221_Project_ShopOnline.DAO;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Text.Json;
using System.Security.Cryptography;

namespace PRN221_Project_ShopOnline.Controllers
{
    public class DefaultController : Controller
    {
        private readonly HttpClient client = null;
        private string url = "";
        private string urlc = "";
        public DefaultController()
        {
            client = new HttpClient();
            var contentType = new MediaTypeWithQualityHeaderValue("application/json");
            client.DefaultRequestHeaders.Accept.Add(contentType);
            url = "https://localhost:7007/product";
            urlc = "https://localhost:7007/category";
        }
        public async Task<ActionResult> Index()
        {
            var view = View("Views/Index.cshtml");
            var response = await client.GetAsync(url);
            var json = await response.Content.ReadAsStringAsync();
            var options = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };
            var  products= JsonSerializer.Deserialize<List<Product>>(json, options);
            var responsec = await client.GetAsync(urlc);
            var jsonc = await responsec.Content.ReadAsStringAsync();
            var optionsc = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };
            var categories = JsonSerializer.Deserialize<List<Category>>(jsonc, optionsc);
            //set to ViewBag
            ViewBag.Categories = categories;
            ViewBag.Products = products;
            //no selected category
            ViewBag.SelectedCategory = 0;

            return view;
        }

        public async Task<ActionResult> FindProductByCategory(int cid)
        {
            var view = View("Views/Index.cshtml");

            //Get list Category
            var responsec = await client.GetAsync(urlc);
            var jsonc = await responsec.Content.ReadAsStringAsync();
            var optionsc = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };
            var categories = JsonSerializer.Deserialize<List<Category>>(jsonc, optionsc);
            //Get list Products by Category
            var response = await client.GetAsync(url+"/categoryId/"+cid);
            var json = await response.Content.ReadAsStringAsync();
            var options = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };
            var products = JsonSerializer.Deserialize<List<Product>>(json, options);

            //set to ViewBag
            ViewBag.Categories = categories;
            ViewBag.Products = products;
            //Selected Category (for different display)
            ViewBag.SelectedCategory = cid;

            return view;
        }

        public async Task<ActionResult> SearchProductByName(string ProductName)
        {
            var view = View("Views/Index.cshtml");

            //Get list Category
            var responsec = await client.GetAsync(urlc);
            var jsonc = await responsec.Content.ReadAsStringAsync();
            var optionsc = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };
            var categories = JsonSerializer.Deserialize<List<Category>>(jsonc, optionsc);

            //Get list Products by Name
            var response = await client.GetAsync(url + "/productname/" + ProductName);
            var json = await response.Content.ReadAsStringAsync();
            var options = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };
            var products = JsonSerializer.Deserialize<List<Product>>(json, options);

            //set to ViewBag
            ViewBag.Categories = categories;
            ViewBag.Products = products;
            //no selected category
            ViewBag.SelectedCategory = 0;

            return view;
        }
    }
}
