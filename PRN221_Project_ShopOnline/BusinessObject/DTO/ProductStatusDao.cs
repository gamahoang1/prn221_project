﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTO
{
    public class ProductStatusDao
    {
        public int StatusId { get; set; }
        public string StatusName { get; set; }

        public virtual ICollection<ProductDao> Products { get; set; }
    }
}
