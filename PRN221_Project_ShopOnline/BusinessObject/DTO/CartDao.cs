﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTO
{
    public class CartDao
    {
        public int UserId { get; set; }
        public int ProductId { get; set; }
        public int? Amount { get; set; }

        public virtual ProductDao Product { get; set; }
        public virtual UserDao User { get; set; }
    }
}
