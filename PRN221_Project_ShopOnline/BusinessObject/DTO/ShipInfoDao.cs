﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTO
{
    public class ShipInfoDao
    {
        public int Id { get; set; }
        public int OrderId { get; set; }
        public string ReceiverName { get; set; }
        public string ReceiverAddress { get; set; }
        public string ReceiverCity { get; set; }
        public string ReceiverPhone { get; set; }
        public string Note { get; set; }

        public virtual OrderDao Order { get; set; }
        public virtual ShipDao City { get; set; }
    }
}
