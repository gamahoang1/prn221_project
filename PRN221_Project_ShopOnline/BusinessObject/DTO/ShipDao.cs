﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTO
{
    public class ShipDao
    {
        public int Id { get; set; }
        public string CityName { get; set; }
        public int? ShipPrice { get; set; }

        public virtual ICollection<ShipInfoDao> ShipInfos { get; set; }
    }
}
