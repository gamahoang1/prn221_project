﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTO
{
    public class UserAddressDao
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Label { get; set; }
        public string Address { get; set; }
        public string City { get; set; }

        public virtual UserDao User { get; set; }
    }
}
