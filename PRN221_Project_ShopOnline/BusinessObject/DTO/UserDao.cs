﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTO
{
    public class UserDao
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public int? RoleId { get; set; }

        public virtual ICollection<ProductDao> Products { get; set; }
        public virtual ICollection<CartDao> Carts { get; set; }
        public virtual ICollection<OrderDao> Orders { get; set; }
        public virtual ICollection<UserAddressDao> Addresses { get; set; }
    }
}
