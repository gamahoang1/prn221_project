﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTO
{
    public class OrderStatusDao
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<OrderDao> Orders { get; set; }
    }
}
