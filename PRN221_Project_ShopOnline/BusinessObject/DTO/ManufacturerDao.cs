﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTO
{
    public class ManufacturerDao
    {
        public int ManufacturerId { get; set; }
        public string ManufacturerName { get; set; }

        public virtual ICollection<ProductDao> Products { get; set; }
    }
}
