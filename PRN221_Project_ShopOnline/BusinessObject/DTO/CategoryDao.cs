﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTO
{
    public class CategoryDao
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string Icon { get; set; }

        public virtual ICollection<ProductDao> Products { get; set; }

        //For Display in CSHTML with HTMLHelper
        public override string ToString()
        {
            return CategoryName;
        }
    }
}
