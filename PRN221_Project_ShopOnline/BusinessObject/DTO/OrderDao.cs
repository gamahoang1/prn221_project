﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTO
{
    public class OrderDao
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public double? TotalPrice { get; set; }
        public string Note { get; set; }
        public int? Status { get; set; }
        public DateTime? DayBuy { get; set; }

        public virtual OrderStatusDao StatusNavigation { get; set; }
        public virtual UserDao User { get; set; }
        public virtual ICollection<OrderDetailDao> OrderDetails { get; set; }
        public virtual ICollection<ShipInfoDao> ShipInfos { get; set; }
    }
}
