﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using ShopOnline_API.Models;

namespace ShopOnline_API.DAO
{
    public class CategoryDAO
    {
        private ElectronicShopPRN221Context context = new ElectronicShopPRN221Context();

        public IEnumerable<Category> GetAllCategories()
        {
            context = new ElectronicShopPRN221Context();

            IEnumerable<Category> categories = context.Categories.ToList();
            return categories;
        }
    }
}
