﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ShopOnline_API.DAO;
using ShopOnline_API.Models;

namespace ShopOnline_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DashboardController : ControllerBase
    {
        [HttpGet]
        [Route("/Sale/numAcc")]
        public IActionResult GetAllAccounts()
        {
            UserDAO userDAO = new UserDAO();
            double numAcc = userDAO.GetAllAccounts().ToList().Count;
            if (numAcc >= 0)
            {
                return Ok(numAcc);
            }
            else
                return BadRequest();
            
        }

        [HttpGet]
        [Route("/Sale/numProd")]
        public IActionResult GetAllProducts()
        {
            //num product
            ProductDAO productDAO = new ProductDAO();
            double numProd = productDAO.GetAllProducts().ToList().Count;
            if (numProd >= 0)
            {
                return Ok(numProd);
            }
            else
                return BadRequest();
        }

        [HttpGet]
        [Route("/Sale/numOrd")]
        public IActionResult GetAllOrder()
        {
            ElectronicShopPRN221Context context = new ElectronicShopPRN221Context();
            double numOrd = context.Orders.ToList().Count;
            if (numOrd >= 0)
            {
                return Ok(numOrd);
            }
            else
                return BadRequest();
        }

        [HttpGet]
        [Route("/Sale/totalRevenue")]
        public IActionResult GetAllOrderDetail()
        {
            //total revenue
            var context = new ElectronicShopPRN221Context();
            List<OrderDetail> allOrderDetail = context.OrderDetails.ToList();
            int totalRevenue = 0;
            foreach (OrderDetail o in allOrderDetail)
            {
                totalRevenue += o.ProductPrice;
            }
            return Ok(totalRevenue);    
        }
      /*  [HttpGet("SetDataToView")]
        private IActionResult SetDataToView()
        {
            //num account, num product, num order, top 3 most sell, top 3 least sell, recent order, count prod by cat
            //num account
            UserDAO userDAO = new UserDAO();
            double numAcc = userDAO.GetAllAccounts().ToList().Count;

            //num product
            ProductDAO productDAO = new ProductDAO();
            double numProd = productDAO.GetAllProducts().ToList().Count;

            //num order
            ElectronicShopPRN221Context context = new ElectronicShopPRN221Context();
            double numOrd = context.Orders.ToList().Count;

            var q = (from h in context.OrderDetails
                     group h by new { h.ProductId } into hh
                     select new OrderDetail
                     {
                         ProductId = hh.Key.ProductId,
                         Quantity = hh.Sum(s => s.Quantity)
                     }).OrderByDescending(i => i.Quantity).ToList<OrderDetail>();
           *//* List<int> a = new List<int>();
            for (int i = 0; i < 3; i++)
            {
                a.Add(q[i].ProductId);
            }*/
          /*  List<Product> mostSellProducts = (List<Product>)context.Products.Where(p => a.Contains(p.ProductId)).ToList();*//*

            // count prod by cate
            var prodCat = (from h in context.Products
                           group h by new { h.CategoryId } into hh
                           select new Product
                           {
                               CategoryId = hh.Key.CategoryId,
                               Amount = hh.Count()
                           }).ToList<Product>();

            foreach (Product prod in prodCat)
            {
                prod.ProductName = context.Categories.FirstOrDefault(c => c.CategoryId == prod.CategoryId).CategoryName;

            }

            //total revenue
            context = new ElectronicShopPRN221Context();
            List<OrderDetail> allOrderDetail = context.OrderDetails.ToList();
            int totalRevenue = 0;
            foreach (OrderDetail o in allOrderDetail)
            {
                totalRevenue += o.ProductPrice;
            }
            var result = new
            {
                NumAcc = numAcc,
                NumProd = numProd,
                NumOrd = numOrd,
              //  MostSellProd = mostSellProducts,
                ProdPerCate = prodCat,
                TotalRevenue = totalRevenue
            }; 
            return Ok(result);
        }*/
    }
}
