﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ShopOnline_API.DAO;
using ShopOnline_API.Models;

namespace ShopOnline_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductManagerController : ControllerBase
    {
        [HttpGet]
        [Route("/product/sellerId/{sellerId}")]
       public IActionResult getProductSeller(int sellerId)
        {
            //Get list products of this Seller for Manage
            ProductDAO productDAO = new ProductDAO();
            List<Product> products = productDAO.GetProductsBySeller(sellerId).ToList();
            return Ok(products);
        }
        
        [HttpPost]
        [Route("/product/ProductManager")]
        public IActionResult addProduct(Product product)
        {
            if (product == null)
            {
                return NotFound();
            }
            //Add to DB
            ProductDAO dao = new ProductDAO();
            bool addResult = dao.AddProduct(product);
            return Ok(addResult);
        }
        [HttpPut]
        [Route("/product/edit/ProductManager/{id}")]
        public IActionResult editProduct(int id,Product product)
        {
            ProductDAO dao = new ProductDAO();
            var p =dao.GetProductByID(id);
            if(p is null)
            {
                return NotFound();
            }
            dao.EditProduct(product);
            return Ok();
        }
        [HttpDelete]
        [Route("/product/delete/ProductManager/{id}")]
        public IActionResult deleteProduct(int id)
        {
            ProductDAO dao = new ProductDAO();
            var p = dao.GetProductByID(id);
            if (p is null)
            {
                return NotFound();
            }
            dao.DeleteProductById(id);
            return Ok();
        }
    }
}
