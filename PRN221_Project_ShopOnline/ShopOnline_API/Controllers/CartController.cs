﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ShopOnline_API.DAO;
using ShopOnline_API.Models;

namespace ShopOnline_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CartController : ControllerBase
    {
        //Show List Cart
        [HttpGet]
        [Route("/cart/GetCartsOfUser/{UserId}")]
        public IActionResult GetCartsOfUser(int UserId)
        {
            //Get user Id fron Session
            
         //    UserId = (int)HttpContext.Session.GetInt32("userId");

            //Get list Cart from DB
            CartDAO cartDAO = new CartDAO();
            List<Cart> carts = cartDAO.GetCartsOfUser(UserId);

            //count total Price
           /* int totalPrice = 0;
            foreach (Cart cart in carts)
            {
                //find the Product of each Cart for display
                ProductDAO productDAO = new ProductDAO();
                cart.Product = productDAO.GetProductByID(cart.ProductId);

                totalPrice += (int)cart.Product.SellPrice * (int)cart.Amount;
            }

            //set data to View
            CartDAO.CartViewModel model = new CartDAO.CartViewModel()
            {
                Carts = carts,
                TotalPrice = cartDAO.GetPriceWithDot(totalPrice)
            };*/

            return Ok(carts);
        }
        //find the Product of each Cart for display
        [HttpGet]
        [Route("/cart/GetProductsOfCartOfUser/{ProductId}")]
        public IActionResult GetProductsOfCartOfUser(int ProductId)
        {
            ProductDAO productDAO = new ProductDAO();
            Product cartprpduct = productDAO.GetProductByID(ProductId);
            return Ok(cartprpduct);
        }

        //Add 1 Product to Cart (Amount=1) {From: Home}
        [HttpGet("/cart/addtocart/{productId}/{UserId}")]
        public IActionResult AddToCart(int productId,int UserId)
        {
        /*    ----------Add To Cart----------*/
            
            //Add to DB (amount = 1)
            CartDAO cartDAO = new CartDAO();
            bool notOutOfStock = cartDAO.AddToCart(UserId, productId, 1);
            return Ok(notOutOfStock);
        }
        //-------------------------------------------------------------------------------------s
        //Add an Amount of Product to Cart {From: ProductDetail}
        [HttpGet("/cart/addtocart/{productId}/{UserId}/{Amount}")]
        public IActionResult AddManyToCart(int ProductId, int Amount,int UserId)
        {
            //   ---Add to Cart---
          
            CartDAO cartDAO = new CartDAO();
            bool notOutOfStock = cartDAO.AddToCart(UserId, ProductId, Amount);
            return Ok(notOutOfStock);
        }

        //Delete all Products in Cart of 1 User
        [HttpDelete]
        [Route("/cart/DeleteProductInCart/{UserId}")]
        public IActionResult DeleteCart(int UserId)
        {
            
            //Delete cart
            CartDAO cartDAO = new CartDAO();
            cartDAO.DeleteCart(UserId);

            //back to show cart
            return Ok("Delete ok");
        }

        //Delete 1 Item in Cart of 1 User
        [HttpDelete]
        [Route("/cart/DeleteProductInCart/{ProductID}/{UserId}")]
        public IActionResult DeleteProductInCart(int ProductID,int UserId)
        {
           
            CartDAO cartDAO = new CartDAO();
            var context = new ElectronicShopPRN221Context();
            Cart cart = context.Carts.SingleOrDefault(c => c.UserId == UserId && c.ProductId == ProductID);
            if(cart != null)
            {
                cartDAO.DeleteProductInCart(UserId, ProductID);
            }
            else
            {
                BadRequest("Product not exist!");
            }
            
            return Ok("Delete One OK");
        }

        //---------------Buy---------------
        [HttpGet]
        [Route("/cart/Ship/{UserId}")]
        public IActionResult Buy(int UserId)
        {
            //ship info
            ShipDAO shipDAO = new ShipDAO();
            List<Ship> ships = shipDAO.GetAllShips();           
            return Ok(ships);
        }

        //---------------Finish---------------
        [HttpDelete]
        [Route("/cart/Finish/{UserId}")]

        public IActionResult Finish(int UserId)
        {
            /*todo: add Cart to Order*/

            /*Delete Cart*/
          
            //delete
            CartDAO cartDAO = new CartDAO();
            cartDAO.DeleteCart(UserId);

            return Ok("OK Finish");
        }
    }
}
