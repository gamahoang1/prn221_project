﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ShopOnline_API.DAO;
using ShopOnline_API.Models;
using System.Security.Cryptography;

namespace ShopOnline_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DefaultController : ControllerBase
    {
        

        [HttpGet]
        [Route("/category")]
        public IActionResult getAllCategory()
        {
            CategoryDAO categoryDao = new CategoryDAO();
             List<Category> category = categoryDao.GetAllCategories().ToList();
            return Ok(category);
        }
        [HttpGet]
        [Route("/product")]
        public IActionResult getAllProduct()
        {
            ProductDAO productDao = new ProductDAO();
            List<Product> products = productDao.GetAllProducts().ToList();
            return Ok(products);
        }
        [HttpGet]
        [Route("/product/categoryId/{categoryId}")]
        public IActionResult getProductByCategory(int categoryId)
        {
            //Get list Products by Category
            ProductDAO productDao = new ProductDAO();
            List<Product> products = productDao.GetProductsByCategory(categoryId).ToList();
            return Ok(products);
        }
        [HttpGet]
        [Route("/product/productname/{productName}")]
        public IActionResult getProductByName(string productName)
        {
            //Get list Products by Name
            ProductDAO productDao = new ProductDAO();
            List<Product> products = productDao.SearchProductByName(productName).ToList();
            return Ok(products);
        }
    }
}
