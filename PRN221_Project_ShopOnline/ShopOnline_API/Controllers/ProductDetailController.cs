﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ShopOnline_API.DAO;
using ShopOnline_API.Models;
namespace ShopOnline_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductDetailController : ControllerBase
    {
        [HttpGet]
        [Route("/product/id/{id}")]
        public IActionResult getProductById(int id)
        {
            //Get product Detail
            ProductDAO dao = new ProductDAO();
            Product p = dao.GetProductByID(id);
            return Ok(p);
        }
    }
}
