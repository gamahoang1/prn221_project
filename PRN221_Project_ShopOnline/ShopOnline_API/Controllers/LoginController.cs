﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ShopOnline_API.DAO;
using ShopOnline_API.Models;

namespace ShopOnline_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        [HttpGet]
        public IActionResult Index()
        {
            //if has Cookies -> set username & password for auto login
            string cookieUsername = "";
            string cookiePassword = "";
            if (HttpContext.Request.Cookies.ContainsKey("username"))
            {
                cookieUsername = HttpContext.Request.Cookies["username"];
                cookiePassword = HttpContext.Request.Cookies["password"];
            }

            return Ok(new { CookieUsername = cookieUsername, CookiePassword = cookiePassword });
        }


        [HttpPost]
        [Route("/user/login/")]
        public IActionResult Login(User u)
        {
            string usename = u.Username;
            string password = u.Password;
            UserDAO dao = new UserDAO();
            var user = dao.GetAccountByNameAndPass(usename,password);
            if (user == null)
            {
                return BadRequest("");
            }
            return Ok(user);
        }

        [HttpPost("Logout")]
        public IActionResult Logout()
        {
            //Remove Session
            HttpContext.Session.Remove("userId");
            HttpContext.Session.Remove("username");
            HttpContext.Session.Remove("password");

            //back to home
            return Ok();
        }

        [HttpPost]
        [Route("/user/Signup")]
        public IActionResult Signup(User x)
        {
            var context = new ElectronicShopPRN221Context();
            //check confirm password
            User user = context.Users.FirstOrDefault(u => u.Email.Equals(x.Email) || u.Username.Equals(x.Username));
            if (user == null)
            {
                    UserDAO dao = new UserDAO();
                    dao.SignUp(x);
                    //back to login
                    return Ok("SignUp Success");     
            }else{
                return BadRequest("User exist!");
            }
            
        }

    }
}
