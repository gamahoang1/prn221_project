﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using ShopOnline_API.DAO;
using ShopOnline_API.Models;

namespace ShopOnline_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountManagerController : ControllerBase
    {

        /* public IActionResult Index()
         {
             SetDataToView();

             return Ok();
         }*/

        [HttpGet]
        [Route("/AccountManager")]

        public IActionResult GetAllUser()
        {
            //Get data from DB
            UserDAO dao = new UserDAO();
            List<User> users = dao.GetAllAccounts().ToList();

            //Set data to View
            return Ok(users);
        }
        [HttpDelete]
        [Route("/AccountManager/Delete/{UserId}")]
        public IActionResult DeleteAccount(int UserId)
        {
            //Delete
            UserDAO dao = new UserDAO();
            bool result = dao.DeleteAccountById(UserId);
            //if delete fail -> notify
            if (!result)
            {
                BadRequest("User not exist");
            }
            return Ok("Delete OK");
        }

        //Go to Edit Form
        [HttpGet()]
        [Route("/AccountManager/EditAccount/{UserID}")]
        public IActionResult EditAccount(int UserID)
        {
            //Get account to edit
            UserDAO dao = new UserDAO();
            User user = dao.GetAccountById(UserID);

            return Ok(user);
        }
        [HttpGet]
        [Route("/user/username/{username}")]
        public IActionResult GetUserByUserName(string username)
        {
            //Get data from DB
            UserDAO dao = new UserDAO();
            User users = dao.GetAccountByName(username);

            //Set data to View
            return Ok(users);
        }
        //Update Account to DB
        [HttpPost]
        [Route("/AccountManager/UpdateAccount")]
        public IActionResult UpdateAccount(User ux)
        {
            try
            {
                //Set data
                User user = new User();
                user.UserId = ux.UserId;
                user.Username =ux.Username;
                user.Email = ux.Email;
                user.Password = ux.Password;
                user.IsSeller = ux.IsSeller;
                user.IsAdmin = ux.IsAdmin;

                //Update to DB
                UserDAO dao = new UserDAO();
                dao.EditAccount(user);
            }
            catch (Exception)
            {
                BadRequest("Can't not update account");
            }
            return Ok("Edit OK");
        }
    }
}
