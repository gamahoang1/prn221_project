﻿using System;
using System.Collections.Generic;

namespace ShopOnline_API.Models
{
    public partial class Category
    {
        public Category()
        {
            Products = new HashSet<Product>();
        }

        public int CategoryId { get; set; }
        public string? CategoryName { get; set; }
        public string? Icon { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}
