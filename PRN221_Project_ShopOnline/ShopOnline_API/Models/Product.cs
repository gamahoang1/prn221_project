﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace ShopOnline_API.Models
{
    public partial class Product
    {
        public Product()
        {
            Carts = new HashSet<Cart>();
            OrderDetails = new HashSet<OrderDetail>();
        }

        public int ProductId { get; set; }
        public string? ProductName { get; set; }
        public string? Description { get; set; }
        public int? OriginalPrice { get; set; }
        public int? SellPrice { get; set; }
        public int? SalePercent { get; set; }
        public string? ImageLink { get; set; }
        public int? CategoryId { get; set; }
        public int? SellerId { get; set; }
        public int? Amount { get; set; }
        public int? StatusId { get; set; }
        public int? ManufacturerId { get; set; }
        public double? Height { get; set; }
        public double? Width { get; set; }
        public double? Weight { get; set; }
        [JsonIgnore]
        public virtual Category? Category { get; set; }
        [JsonIgnore]
        public virtual Manufacturer? Manufacturer { get; set; }
        [JsonIgnore]
        public virtual User? Seller { get; set; }
        [JsonIgnore]
        public virtual ProductStatus? Status { get; set; }
        [JsonIgnore]
        public virtual ICollection<Cart> Carts { get; set; }
        [JsonIgnore]
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
    }
}
